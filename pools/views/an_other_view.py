""" Cornice services.
"""
from cornice import Service


an_other_hello = Service(name='hello2', path='/isd', description="Simplest app")


@an_other_hello.get()
def get_info(request):
    """Returns Hello in JSON."""
    return {'Hello': 'This the Python world'}