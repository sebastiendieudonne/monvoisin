""" Cornice services.
"""
from cornice import Service


hello = Service(name='hello', path='/', description="Simplest app")


@hello.get()
def get_info(request):
    """Returns Hello in JSON."""
    return {'Hello': 'World'}


@hello.post()
def post_info(request):
    """Insert some JSON."""
    return {'Message': 'I have insert your JSON'}
