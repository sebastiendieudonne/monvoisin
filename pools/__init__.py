"""Main entry point
"""
from pyramid.config import Configurator


def main(global_config, **settings):
    config = Configurator(settings=settings)
    config.include("cornice")
    config.scan("pools.views")
    config.scan("pools.an_other_view")
    return config.make_wsgi_app()
